package com.dl.HelpDesk.controllers;

import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.TicketsMode;
import com.dl.HelpDesk.domain.service.abstracts.TicketService;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import com.dl.HelpDesk.utils.TicketView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/rest")
public class HelpDeskRestController {
    @Autowired
    private TicketService ticketService;
    @Autowired
    private UserService userService;

    @JsonView(TicketView.TicketList.class)
    @RequestMapping(value = "/tickets/{mode}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getTickets(Ticket ticket,
                                                          Pageable pageable,
                                                          @AuthenticationPrincipal UserDetails principal,
                                                          @PathVariable(value = "mode") TicketsMode ticketsMode) {
        User currentUser = userService.findUserByEmail(principal.getUsername());
        Page ticketPage = ticketService.getTicketPage(currentUser, ticket, pageable, ticketsMode);
        return new ResponseEntity<>(TicketView.convertTicketPageForView(ticketPage), HttpStatus.OK);
    }


}
