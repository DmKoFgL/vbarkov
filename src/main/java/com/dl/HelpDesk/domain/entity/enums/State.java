package com.dl.HelpDesk.domain.entity.enums;

public enum State {
    DRAFT,
    NEW,
    APPROVED,
    DECLINED,
    IN_PROGRESS,
    DONE,
    CANCELED
}
