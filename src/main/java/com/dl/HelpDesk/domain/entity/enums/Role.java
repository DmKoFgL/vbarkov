package com.dl.HelpDesk.domain.entity.enums;

public enum Role {
    EMPLOYEE, MANAGER, ENGINEER;

}
