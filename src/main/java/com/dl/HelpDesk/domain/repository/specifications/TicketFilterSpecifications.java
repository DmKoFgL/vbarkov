package com.dl.HelpDesk.domain.repository.specifications;

import com.dl.HelpDesk.Exceptions.UnexpectedUserRoleException;
import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.Ticket_;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.Role;
import com.dl.HelpDesk.domain.entity.enums.State;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.HashSet;

public class TicketFilterSpecifications {
    public static Specification<Ticket> myFilter(User user) {
        Specification<Ticket> result;
        switch (user.getRole()) {
            case EMPLOYEE:
                result = ownerFilter(user);
                break;
            case MANAGER:
                result = ownerFilter(user).or(approverFilter(user).and(onStatesFilter(State.APPROVED)));
                break;
            case ENGINEER:
                result = assigneeFilter(user);
                break;
            default:
                throw new UnexpectedUserRoleException("Don't expected role \"" + user.getRole() + "\"");
        }
        return result;
    }

    public static Specification<Ticket> myFilter(User user, Ticket ticket) {
        return myFilter(user).and(filterByExample(ticket));
    }

    public static Specification<Ticket> filterByExample(Ticket ticket) {

        return new Specification<Ticket>() {
            @Override
            public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Collection<Predicate> predicates = new HashSet<>();
                if (ticket.getId() != null) {
                    predicates.add(criteriaBuilder.equal(root.get(Ticket_.id), ticket.getId()));
                }
                if (ticket.getName() != null) {
                    predicates.add(criteriaBuilder.like(root.get(Ticket_.name), "%" + ticket.getName() + "%"));
                }
                if (ticket.getDesiredResolutionDate() != null) {
                    predicates.add(criteriaBuilder.equal(root.get(Ticket_.desiredResolutionDate), ticket.getDesiredResolutionDate()));
                }
                if (ticket.getUrgency() != null) {
                    predicates.add(criteriaBuilder.equal(root.get(Ticket_.urgency), ticket.getUrgency()));
                }
                if (ticket.getState() != null) {
                    predicates.add(criteriaBuilder.equal(root.get(Ticket_.state), ticket.getState()));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }
        };


    }

    public static Specification<Ticket> allFilter(User user) {
        Specification<Ticket> result;
        switch (user.getRole()) {
            case EMPLOYEE:
                result = ownerFilter(user);
                break;
            case MANAGER:
                result = ownerFilter(user)
                        .or(ownerRoleFilter(Role.EMPLOYEE).and(onStatesFilter(State.NEW)))
                        .or(approverFilter(user)
                                .and(onStatesFilter(State.APPROVED, State.DECLINED, State.CANCELED, State.IN_PROGRESS)));
                break;
            case ENGINEER:
                result = (assigneeFilter(user).and(onStatesFilter(State.IN_PROGRESS, State.DONE)))
                        .or((ownerRoleFilter(Role.EMPLOYEE).or(ownerRoleFilter(Role.MANAGER)))
                                .and(onStatesFilter(State.APPROVED)));
                break;
            default:
                throw new UnexpectedUserRoleException("Don't expected role \"" + user.getRole() + "\"");
        }
        return result;

    }

    private static Specification<Ticket> ownerFilter(User owner) {
        return new Specification<Ticket>() {
            @Override
            public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get(Ticket_.owner), owner);
            }
        };
    }

    private static Specification<Ticket> ownerRoleFilter(Role role) {
        return new Specification<Ticket>() {
            @Override
            public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Ticket, User> ticketJoin = root.join(Ticket_.owner);
                return criteriaBuilder.equal(root.get("owner").get("role"), role);
            }
        };
    }

    private static Specification<Ticket> approverFilter(User user) {
        return new Specification<Ticket>() {
            @Override
            public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get(Ticket_.approver), user);
            }
        };
    }

    private static Specification<Ticket> assigneeFilter(User user) {
        return new Specification<Ticket>() {
            @Override
            public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get(Ticket_.assignee), user);
            }
        };
    }

    private static Specification<Ticket> onStatesFilter(State... states) {
        return new Specification<Ticket>() {
            @Override
            public Predicate toPredicate(Root<Ticket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path ticketState = root.get(Ticket_.state);
                return ticketState.in(states);
            }
        };
    }


}
