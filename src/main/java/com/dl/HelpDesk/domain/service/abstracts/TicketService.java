package com.dl.HelpDesk.domain.service.abstracts;

import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.TicketsMode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TicketService {
    Page getTicketPage(User user, Pageable pageable);
    Page getTicketPage(User user, Pageable pageable,TicketsMode filterMode);

    Page getTicketPage(User user, Ticket ticketExample, Pageable pageable);

    Page getTicketPage(User user, Ticket ticketExample, Pageable pageable, TicketsMode filterMode);
}
