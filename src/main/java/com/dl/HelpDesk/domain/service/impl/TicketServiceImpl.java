package com.dl.HelpDesk.domain.service.impl;

import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.repository.abstracts.TicketRepository;
import com.dl.HelpDesk.domain.repository.specifications.TicketFilterSpecifications;
import com.dl.HelpDesk.domain.service.abstracts.TicketService;
import com.dl.HelpDesk.domain.entity.enums.TicketsMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class TicketServiceImpl implements TicketService {
    @Autowired
    private TicketRepository ticketRepository;

    public Page getTicketPage(User user, Pageable pageable) {

        return getTicketPage(user, new Ticket(), pageable, TicketsMode.ALL);
    }

    @Override
    public Page getTicketPage(User user, Pageable pageable, TicketsMode filterMode) {

        return getTicketPage(user, new Ticket(), pageable, filterMode);
    }

    public Page getTicketPage(User user, Ticket ticket, Pageable pageable) {

        return getTicketPage(user, ticket, pageable, TicketsMode.ALL);
    }

    @Override
    public Page getTicketPage(User user, Ticket ticket, Pageable pageable, TicketsMode filterMode) {
        Specification<Ticket> specification = TicketFilterSpecifications.filterByExample(ticket);
        switch (filterMode) {
            case MY:

                specification = specification.and(TicketFilterSpecifications.myFilter(user));
                break;
            case ALL:
                specification = specification.and(TicketFilterSpecifications.allFilter(user));
                break;
            default:
                throw new RuntimeException("Unexpected filter mode " + filterMode);
        }

        return ticketRepository.findAll(specification, pageable);
    }


}