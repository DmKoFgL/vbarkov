package com.dl.HelpDesk.domain.service.abstracts;

import com.dl.HelpDesk.domain.entity.User;

import java.util.List;

public interface UserService {
    User getUserById(Long id);

    List<User> getAllUsers();

    void addUser(User user);

    User findUserByEmail(String email);
}
