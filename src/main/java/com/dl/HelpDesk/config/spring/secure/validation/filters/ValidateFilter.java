package com.dl.HelpDesk.config.spring.secure.validation.filters;

import com.dl.HelpDesk.config.spring.secure.validation.PasswordValidator;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ValidateFilter extends UsernamePasswordAuthenticationFilter {


    public ValidateFilter() {
        super();
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String password = obtainPassword(request);
        if (this.requiresAuthentication(request, response)) {
            PasswordValidator validator = new PasswordValidator();
            if (!validator.isValid(password,null)) {
                BadCredentialsException exception = new BadCredentialsException("");
                unsuccessfulAuthentication(request, response, exception);
            }
        }
        chain.doFilter(request, response);
    }

}
