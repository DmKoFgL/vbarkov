package com.dl.HelpDesk.Exceptions;

public class UnexpectedUserRoleException extends RuntimeException {
    public UnexpectedUserRoleException(String message) {
        super(message);
    }
}
