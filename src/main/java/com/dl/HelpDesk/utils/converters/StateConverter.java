package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.domain.entity.enums.State;
import org.springframework.core.convert.converter.Converter;

public class StateConverter implements Converter<String, State> {
    @Override
    public State convert(String s) {
        return State.valueOf(s.toUpperCase());
    }
}
