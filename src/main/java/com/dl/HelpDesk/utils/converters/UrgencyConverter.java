package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.domain.entity.enums.Urgency;
import org.springframework.core.convert.converter.Converter;

public class UrgencyConverter implements Converter<String, Urgency> {
    @Override
    public Urgency convert(String s) {
        return Urgency.valueOf(s.toUpperCase());

    }
}
