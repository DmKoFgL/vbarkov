package com.dl.HelpDesk.utils;

import org.springframework.data.domain.Page;

import java.util.HashMap;
import java.util.Map;

public class TicketView {
    public static class TicketList {
    }

    public static Map<String, Object> convertTicketPageForView(Page ticketPage) {
        Map<String, Object> map = new HashMap<>();
        map.put("tickets", ticketPage.getContent());
        map.put("pageCount", ticketPage.getTotalPages());
        return map;
    }
}
