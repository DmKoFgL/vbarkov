package com.dl.HelpDesk.utils.converters;

import com.dl.HelpDesk.domain.entity.enums.TicketsMode;
import org.junit.Assert;
import org.junit.Test;

public class ConvertersTest {


    @Test
    public void ticketsModeConverterPositive() {
        TicketsModeConverter converter = new TicketsModeConverter();

        Assert.assertEquals(TicketsMode.ALL, converter.convert("ALL"));
        Assert.assertEquals(TicketsMode.ALL, converter.convert("all"));
        Assert.assertEquals(TicketsMode.MY, converter.convert("My"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ticketsModeConverterNegative() {
        TicketsModeConverter converter = new TicketsModeConverter();
        converter.convert("Allive");
    }

}