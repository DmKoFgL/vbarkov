package com.dl.HelpDesk.domain.service.impl;

import com.dl.HelpDesk.config.spring.ApplicationConfig;
import com.dl.HelpDesk.config.spring.ConnectDatabaseConfig;
import com.dl.HelpDesk.domain.entity.Ticket;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.service.abstracts.TicketService;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import com.dl.HelpDesk.domain.entity.enums.TicketsMode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, ConnectDatabaseConfig.class})
@WebAppConfiguration
@Transactional
@Rollback()
public class TicketServiceImplTest {
    @Autowired
    private TicketService ticketService;
    @Autowired
    private UserService userService;

    @Test
    public void getTicketPageForEmployer() {
        User user = userService.getUserById(1l);
        Page p = ticketService.getTicketPage(user, new PageRequest(0, 10));
        Assert.assertNotEquals(p.getContent().size(), 0);
    }

    @Test
    public void getTicketPageForManager() {
        User user = userService.getUserById(3l);
        Page ticketPage = ticketService.getTicketPage(user, new PageRequest(0, 10));
        Assert.assertNotEquals(ticketPage.getContent().size(), 0);
    }

    @Test
    public void getMyTicketPageForManager() {
        User user = userService.getUserById(3l);
        Page ticketPage = ticketService.getTicketPage(user, new PageRequest(0, 10), TicketsMode.MY);
        Assert.assertNotEquals(ticketPage.getContent().size(), 0);
    }

    @Test
    public void getFilteredTicketPageForManager() {
        User user = userService.getUserById(3l);
        Ticket ticket = new Ticket();
        ticket.setName("a");
        Page ticketPage = ticketService.getTicketPage(user, ticket, new PageRequest(0, 10), TicketsMode.ALL);
        Assert.assertNotEquals(ticketPage.getContent().size(), 0);
    }
}