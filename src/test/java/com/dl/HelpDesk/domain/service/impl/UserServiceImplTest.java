package com.dl.HelpDesk.domain.service.impl;

import com.dl.HelpDesk.Exceptions.UserNotFoundException;
import com.dl.HelpDesk.config.spring.ApplicationConfig;
import com.dl.HelpDesk.config.spring.ConnectDatabaseConfig;
import com.dl.HelpDesk.domain.entity.User;
import com.dl.HelpDesk.domain.entity.enums.Role;
import com.dl.HelpDesk.domain.service.abstracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, ConnectDatabaseConfig.class})
@WebAppConfiguration
@Transactional
@Rollback()
public class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    public void getAllUsers() {
        List<User> users = userService.getAllUsers();
        Assert.assertNotEquals(users.size(), 0);
    }

    @Test
    public void findUserByUsername() {
        User user = userService.findUserByEmail("user1_mogilev@yopmail.com");
        Assert.assertNotNull(user);
    }

    @Test(expected = UserNotFoundException.class)
    public void findUserByUsernameNegative() {
        User user = userService.findUserByEmail("user_not_exists@yopmail.com");
    }

    @Test
    public void getUserById() {
        User user = userService.getUserById(1L);
        Assert.assertEquals("user1_mogilev@yopmail.com", user.getEmail());
    }

    @Test
    public void addUser() {
        User user = new User("test1", "test2", "test@mail.com", Role.EMPLOYEE, "pass");
        userService.addUser(user);
        User userFromDb = userService.findUserByEmail(user.getEmail());
        Assert.assertEquals(userFromDb.getId(), user.getId());
    }
}