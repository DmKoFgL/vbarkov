import axios from 'axios'
axios.defaults.withCredentials =true;
export const authenticated = (user) => dispatch => {

    axios('http://localhost:8088/oauth/authorize?response_type=token&client_id=react-client&redirect_uri=localhost:3000/login', {
        method: 'POST',
        auth: user
    })
        .then(res => {
            dispatch({
               type: 'LOGIN_SUCCESS',
                payload: res
            });
        }).catch(res => {
            dispatch({
                type: 'LOGIN_REQUEST_FAILED',
                payload: res.response
            });
        })
};
export const checkAuthenticated = () => dispatch => {

    axios('http://localhost:8080/rest/hi', {
        method: 'POST'
    })
        .then(res => {
            dispatch({
               type: 'IS_AUTHORIZE',
                payload: res
            });
        }).catch(res => {
            dispatch({
                type: 'NOT_AUTHORIZE',
                payload: res.response
            });
        })
};