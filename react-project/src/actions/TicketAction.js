import axios from 'axios'
export const getTickets = (mode = "ALL", sorter, filter,pagination) => dispatch => {
    let token = JSON.parse(window.localStorage.getItem("OAuthProvider_token"));
    let params = { access_token: token.access_token };

    if (filter !== undefined && filter.field !== "" && filter !== {}) {
     Object.assign(params,filter)
    }
    if (sorter !== undefined) {
        params.sort = sorter.field + "," + (sorter.isAsc ? "asc" : "desc");
    }
    if(pagination!==undefined){
        params.page=pagination.activePage;
        params.size=pagination.pageSize;
        
    }
    axios.get('http://localhost:8088/rest/tickets/' + mode, {
        params: params
    }).then(res => {
        dispatch({
            type: 'GET_TICKETS_SUCCESS',
            payload: res.data
        });
    }).catch(err => {
        dispatch({
            type: 'REQUEST_FAILED'
        });
    })
};