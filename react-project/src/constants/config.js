
export const providerConfig = {
    clientId        : 'react-client',
    redirectUri     :  window.location.origin + '/login',
    authorizationUrl: 'http://localhost:8088/oauth/authorize',
    scope           :'',
    width           : 1080,
    height          : 640
  };