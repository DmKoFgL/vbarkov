const initialState = {
    credentials: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                credentials: action.data.payload
            };
        case 'LOGIN_REQUEST_FAILED': {
            alert(action.payload);
            break;
        }
        default:
    }
    return state;
};
