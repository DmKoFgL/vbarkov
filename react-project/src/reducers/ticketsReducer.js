const initialState = {
    tickets: [],
    pageCount:0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_TICKETS_SUCCESS':
            return {
                ...state,
                tickets: action.payload.tickets,
                pageCount: action.payload.pageCount
            };
        case 'MY_TICKETS_LOADED':
            return {
                ...state,
                tickets: action.payload
            };
        case 'REQUEST_FAILED': {
            console.log('REQUEST_FAILED');
            break;
        }
        default:
    }
    return state;
};
