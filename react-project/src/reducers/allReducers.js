
import ticketsReducers from './TicketsReducer'
import { combineReducers } from 'redux'

const allReducers = combineReducers({
    ticketList: ticketsReducers
});

export default allReducers;