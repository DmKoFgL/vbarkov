import React from 'react';
import Logout from './components/Logout'

const Navbar = (props) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light mb-3" >
            <div className="collapse navbar-collapse">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">

                    <li className="nav-item">
                        <a className="nav-link" href="/login">Login</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/tickets">Tickets</a>
                    </li>
                    <li className="nav-item">
                          <Logout />
                    </li>
                </ul>

            </div>
        </nav>

    );
};
export default Navbar