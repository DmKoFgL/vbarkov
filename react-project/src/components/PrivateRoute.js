import React from 'react';
import { Route, Redirect } from 'react-router-dom'
export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        window.localStorage.getItem('OAuthProvider_token') != null
            ? <Component {...props} />
            : <Redirect to='/login' />
    )} />
)
export default PrivateRoute;