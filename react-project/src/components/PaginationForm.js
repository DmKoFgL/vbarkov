import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

export default class PaginationForm extends Component {
    handleChange(event) {
        this.setState({ value: event.target.value });
    }
    render() {
        return (
            <div className="container w-75">
                <ReactPaginate
                    initialPage={this.props.activePage}
                    pageCount={this.props.pageCount}
                    pageRangeDisplayed={2}
                    marginPagesDisplayed={3}
                    previousLabel="previous"
                    nextLabel="next"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    activeClassName="active"
                    containerClassName="pagination"
                    previousClassName="page-link"
                    nextClassName="page-link"
                    onPageChange={this.props.onPageChange}
                    disabledClassName="page-item disabled"
                />
                <form>
                    <label>
                        Page size:
          <input type="number" value={this.props.value} placeholder="Page size" onChange={this.props.ChangePageSize} />
                    </label>

                </form>
            </div>
        );
    }
}