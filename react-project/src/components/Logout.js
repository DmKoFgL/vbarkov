import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { checkAuthenticated } from '../actions/LoginAction'
import { logout } from '../actions/LogoutAction'


class Logout extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.state={ token: window.localStorage.getItem('OAuthProvider_token') };
    }

    componentDidMount() {
       this.setState({ token: window.localStorage.getItem('OAuthProvider_token') });
 
    }
    logout() {
        // this.props.logout()
        window.localStorage.removeItem('OAuthProvider_token');
        window.location ="/home"
    }
    render() {
        return (
            <div>
                {this.state.token==null ? "":
                    <button onClick={this.logout} className="btn btn-dark">Logout</button> 
                }
            </div>
        )
    };

}
function mapStateToProps(state) {
    return {
        ...state,
        auth: state.authBackend
    };
};
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ checkAuthenticated, logout }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);