import React, { Component } from 'react';

export default class TicketsForm extends Component {
    render() {
        return (
            <div className="container w-75">
                <div className="input-group-prepend">
                    <input className={this.props.btnClassAllTickets} type="button" value="All tickets"
                        onClick={this.props.onGetAllTickets} />
                    <input className={this.props.btnClassMyTickets} type="button" value="My tickets"
                        onClick={this.props.onGetMyTickets} />
                </div>
            </div>
        )
    }
}