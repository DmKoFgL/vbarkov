import React, { Component } from 'react';
import Moment from 'react-moment';
import DatePicker from "react-datepicker";
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

export default class TicketList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            desiredDate: "",
            filter: {},
            statusOptions: [{ value: 'DRAFT', label: 'DRAFT' },
            { value: 'NEW', label: 'NEW' },
            { value: 'APPROVED', label: 'APPROVED' },
            { value: 'DECLINED', label: 'DECLINED' },
            { value: 'DONE', label: 'DONE' },
            { value: 'CANCELED', label: 'CANCELED' },
            { value: 'IN_PROGRESS', label: 'IN PROGRESS' }],
            selectedState: "",
            selectedUrgency: "",
            urgencyOptions: [{ value: 'CRITICAL', label: 'CRITICAL' },
            { value: 'HIGH', label: 'HIGH' },
            { value: 'AVERAGE', label: 'AVERAGE' },
            { value: 'LOW', label: 'LOW' }]
        };
        this.onSortClick = this.onSortClick.bind(this);
        this.onFilter = this.onFilter.bind(this);
        this.desiredResolutionDateChanged = this.desiredResolutionDateChanged.bind(this);
        this.ticketStateChanged = this.ticketStateChanged.bind(this);
        this.urgencyChanged = this.urgencyChanged.bind(this);


    }
    onSortClick(e) {
        this.props.onSort(e.target.id);
    }
    onFilter(e) {
        let newfilter = this.state.filter;

        if (e.target.value === "") {
            delete newfilter[e.target.name];
        } else {
            newfilter[e.target.name] = e.target.value;
            this.props.onFilter({ filter: newfilter });
        }
    }
    desiredResolutionDateChanged(date) {
        let newfilter = this.state.filter;
        newfilter['desiredResolutionDate'] = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
        this.setState({ filter: newfilter, desiredResolutionDate: date });
        this.props.onFilter({ filter: newfilter });
    }
    ticketStateChanged(state) {
        let newfilter = this.state.filter;
        newfilter.state = state.value;
        this.setState({ filter: newfilter, selectedState: state });
        this.props.onFilter({ filter: newfilter });
    }
    urgencyChanged(urgency) {
        let newfilter = this.state.filter;
        newfilter.urgency = urgency.value;
        this.setState({ filter: newfilter, selectedUrgency: urgency });
        this.props.onFilter({ filter: newfilter });
    }
    render() {
        return (
            <div className="container w-75">
                <table className="table table-hover table-bordered">
                    <thead>
                        <tr className="table-active">
                            <th><span className="btn" id='id' onClick={this.onSortClick}>ID</span></th>
                            <th><span className="btn" id='Name' onClick={this.onSortClick}>Name</span></th>
                            <th><span className="btn" id='desiredResolutionDate' onClick={this.onSortClick}>Desired Date</span></th>
                            <th><span className="btn" id='Urgency' onClick={this.onSortClick}>Urgency</span></th>
                            <th><span className="btn" id='State' onClick={this.onSortClick}>State</span></th>
                        </tr>
                        <tr>
                            <th><input type="number" className="form-control" name="id" placeholder="ID" onChange={this.onFilter} /></th>
                            <th><input type="text" className="form-control" name="Name" placeholder="Name" onChange={this.onFilter} /></th>
                            <th><DatePicker onChange={this.desiredResolutionDateChanged} selected={this.state.desiredResolutionDate} /></th>
                            <th> <Dropdown options={this.state.urgencyOptions} onChange={this.urgencyChanged} value={this.state.selectedUrgency} placeholder="Urgency" /></th>
                            <th><Dropdown options={this.state.statusOptions} onChange={this.ticketStateChanged} value={this.state.filter.selectedState} placeholder="State" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.tickets && this.props.tickets.map(ticket => (
                                <tr key={ticket.id}>
                                    <td>{ticket.id}</td>
                                    <td><a href={"/tickets/" + ticket.id}>{ticket.name}</a></td>
                                    <td><Moment format="DD MMM YYYY">{ticket.desiredResolutionDate}</Moment></td>
                                    <td>{ticket.urgency}</td>
                                    <td>{ticket.state}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        )
    }

}