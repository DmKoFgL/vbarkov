import React, { Component } from 'react';
import Navbar from './navbar'
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Switch from '../node_modules/react-router-dom/Switch';
import Login from './components/Login'
import './bootstrap.min.css'
import Tickets from './containers/TicketsContainer';
import Logout from './components/Logout';
import PrivateRoute from "./components/PrivateRoute"

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Navbar />
          <Router>
            <Switch>
              <Route exact path='/login' component={Login} />
              <Route exact path='/Logout' component={Logout} />
              <PrivateRoute exact path='/tickets' component={Tickets} />
            </Switch>
          </Router>
        </div>
      </Provider>
    );
  }
}


export default App;
