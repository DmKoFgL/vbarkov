import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTickets } from '../actions/TicketAction';
import TicketsForm from '../components/TicketsForm';
import TicketList from '../components/TicketList';
import PaginationForm from '../components/PaginationForm';

class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            sorter: { field: "id", isAsc: true },
            filter: { field: "", value: "" },
            formState: "ALL_TICKETS",
            pagination: { activePage: 0, pageCount: 2 }
        });
        this.onGetMyTickets = this.onGetMyTickets.bind(this);
        this.onGetAllTickets = this.onGetAllTickets.bind(this);

        this.onSort = this.onSort.bind(this);
        this.onFilterAndSort = this.onFilterAndSort.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.onChangePageSize = this.onChangePageSize.bind(this);
    }
    onGetMyTickets() {
        this.setState({ formState: "MY_TICKETS" });
        this.onFilterAndSort({ mode: "MY" });
    }
    onGetAllTickets() {
        this.setState({ formState: "ALL_TICKETS" });
        this.onFilterAndSort({ mode: "ALL" });
    }
    onChangePage(newPage) {
        let pagination = { ...this.state.pagination, activePage: newPage.selected };
        this.setState({ pagination: pagination });
        this.onFilterAndSort({ pagination: pagination });
    }
    onChangePageSize(newPageSize) {
        let newPagination = { ...this.state.pagination, pageSize: newPageSize.target.value };
        this.setState({ pagination: newPagination });
        this.onFilterAndSort({ pagination: newPagination });
    }

    onSort(field) {
        let newSorter = this.state.sorter;

        if (newSorter.field === field) {
            newSorter.isAsc = !newSorter.isAsc;
        } else {
            newSorter.field = field;
            newSorter.isAsc = true;
        }
        this.onFilterAndSort({ sorter: newSorter });
        this.setState({ sorter: newSorter });
    }

    onFilterAndSort({ mode, sorter, filter, pagination }) {
        if (mode === undefined) {
            mode = this.state.formState === "ALL_TICKETS" ? "ALL" : "MY";
        }
        if (sorter === undefined) {
            sorter = this.state.sorter;
        }
        if (filter === undefined) {
            filter = this.state.filter;
        }
        if (pagination === undefined) {
            pagination = this.state.pagination;
        }
        this.props.getTickets(mode, sorter, filter, pagination);
    }

    render() {
        return (
            <div>
                <TicketsForm onFilter={this.onFilter} onGetAllTickets={this.onGetAllTickets}
                    onGetMyTickets={this.onGetMyTickets}
                    btnClassAllTickets={this.state.formState === "ALL_TICKETS" ? "btn btn-primary mr-2 w-50" : "btn btn-outline-primary mr-2 w-50"}
                    btnClassMyTickets={this.state.formState === "MY_TICKETS" ? "btn btn-primary mr-2 w-50" : "btn btn-outline-primary mr-2 w-50"} />
                <TicketList tickets={this.props.tickets} onSort={this.onSort} onFilter={this.onFilterAndSort} />
                <PaginationForm
                    activePage={this.state.pagination.activePage}
                    onPageChange={this.onChangePage}
                    pageCount={this.props.pageCount}
                    ChangePageSize={this.onChangePageSize} />
            </div>
        );
    }

}
function mapStateToProps(state) {
    return {
        tickets: state.ticketList.tickets,
        pageCount: state.ticketList.pageCount
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTickets
    }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Tickets);